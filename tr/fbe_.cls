%%
%% This is file `report.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% classes.dtx  (with options: `report')
%% 
%% This is a generated file.
%% 
%% Copyright 1993 1994 1995 1996 1997 1998 1999 2000 2001 2002 2003 2004 2005 2006 2007 2008 2009
%% The LaTeX3 Project and any individual authors listed elsewhere
%% in this file.
%% 
%% This file was generated from file(s) of the LaTeX base system.
%% --------------------------------------------------------------
%% 
%% It may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3c
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%% 
%% This file has the LPPL maintenance status "maintained".
%% 
%% This file may only be distributed together with a copy of the LaTeX
%% base system. You may however distribute the LaTeX base system without
%% such generated files.
%% 
%% The list of all files belonging to the LaTeX base distribution is
%% given in the file `manifest.txt'. See also `legal.txt' for additional
%% information.
%% 
%% The list of derived (unpacked)\tableofcontents files belonging to the distribution
%% and covered by LPPL is defined by the unpacking scripts (with
%% extension .ins) which are part of the distribution.
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{fbe}
              [2015/11/25 v1.0h
 Standard LaTeX document class]
\usepackage{amssymb}
%\RequirePackage{fontspec}

%\RequirePackage[turkish]{babel}

\RequirePackage{fmtcounttr}  
%!!!!!!!!!!!!!!!!!!!!!!!!
% fc-british.def ve fc-english.def dosyalarını türkçe karakterler için düzenledim

%\RequirePackage{etoolbox}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{setspace}

\newcommand\@ptsize{} 
\newif\if@restonecol
\newif\if@titlepage
\@titlepagetrue
\newif\if@openright
\newif\if@PhD
\newif\if@withcoadvisor
\newif\if@coadvisorsigned
\@withcoadvisorfalse
\@coadvisorsignedfalse

\@PhDfalse

\DeclareOption{a4paper}
   {\setlength\paperheight{297mm}
    \setlength\paperwidth{210mm}}

\DeclareOption{12pt}{\renewcommand\@ptsize{2}}

\DeclareOption{oneside}{\@twosidefalse \@mparswitchfalse}

\DeclareOption{final}{\setlength\overfullrule{0pt}}

\DeclareOption{titlepage}{\@titlepagetrue}

\DeclareOption{openany}{\@openrightfalse}

\DeclareOption{onecolumn}{\@twocolumnfalse}

\DeclareOption{msc}{\@PhDfalse}

\DeclareOption{withcoadvisor}{\@withcoadvisortrue}

\DeclareOption{coadvisorsigned}{\@coadvisorsignedtrue}

% default options for the document.
\ExecuteOptions{a4paper,12pt,oneside,onecolumn,final,openany} 

\ProcessOptions
\input{size1\@ptsize.clo}

% https://en.wikibooks.org/wiki/LaTeX/Page_Layout
\setlength\voffset{-1in} 
\setlength\hoffset{-1in}
\setlength\marginparwidth{0mm}
\setlength\oddsidemargin{40mm}
\setlength\evensidemargin{40mm}
\setlength\marginparsep{0mm}
\setlength\headheight{0mm}
\setlength\headsep{0mm}

% sağdan 25mm; soldan 40mm; toplam genişlik 210mm 
\setlength\textwidth {145mm} 

% üstten boşluk 30mm
\setlength\topmargin{30mm}

% üstten 30mm; alttan 30mm; toplam yükseklik 297mm
\setlength\textheight {237mm}

\setlength\footskip{13mm} 
\setlength\parindent{5mm}

\setlength\parskip {\baselineskip}
%satır aralıkları 1.5 olmalıdır
\renewcommand{\baselinestretch}{1.5}

\renewcommand{\@afterindentfalse}{\@afterindenttrue}
\if@twoside
  \def\ps@headings{%
      \let\@oddfoot\@empty\let\@evenfoot\@empty
      \def\@evenhead{\thepage\hfil\slshape\leftmark}%
      \def\@oddhead{{\slshape\rightmark}\hfil\thepage}%
      \let\@mkboth\markboth
    \def\chaptermark##1{%
      \markboth {\MakeUppercase{%
        \ifnum \c@secnumdepth >\m@ne
            \@chapapp\ \thechapter. \ %
        \fi
        ##1}}{}}%
    \def\sectionmark##1{%
      \markright {\MakeUppercase{%
        \ifnum \c@
         >\z@
          \thesection. \ %
        \fi
        ##1}}}}
\else
  \def\ps@headings{%
    \let\@oddfoot\@empty
    \def\@oddhead{{\slshape\rightmark}\hfil\thepage}%
    \let\@mkboth\markboth
    \def\chaptermark##1{%
      \markright {\MakeUppercase{%
        \ifnum \c@secnumdepth >\m@ne
            \@chapapp\ \thechapter. \ %
        \fi
        ##1}}}}
\fi
\def\ps@myheadings{%
    \let\@oddfoot\@empty\let\@evenfoot\@empty
    \def\@evenhead{\thepage\hfil\slshape\leftmark}%
    \def\@oddhead{{\slshape\rightmark}\hfil\thepage}%
    \let\@mkboth\@gobbletwo
    \let\chaptermark\@gobble
    \let\sectionmark\@gobble
    }
    
\if@titlepage  
  \newcommand\maketitle{
  \begin{titlepage}%
  \let\footnotesize\small
  \let\footnoterule\relax
  \let\footnote\thanks
% Dış Kapak
  \begin{center}
  	{\fontsize{14pt}{\baselineskip}\selectfont
  		\textbf{DOKUZ EYLÜL ÜNİVERSİTESİ\\ \hspace{1mm} FEN BİLİMLERİ ENSTİTÜSÜ}
  		\vspace{3.\baselineskip} %3 satır, 14 puntoluk boşluk
  		}% aşağıdaki boş satırı silme. 
  		
    {\fontsize{18pt}{\baselineskip}\selectfont
    	\textbf{\MakeUppercase{\@title}}\par
    	\vspace{4.6\baselineskip}}%
    	
    {\fontsize{14pt}{\baselineskip}\selectfont
        \textbf{\@author}}%
    
    \vfill
    {\fontsize{12pt}{\baselineskip}\selectfont 
    	\textbf{\@date}\\ \textbf{İZMİR}}%    
  \end{center}

%% İngilizce Dış Kapak
%\clearpage
%\thispagestyle{empty}
%	\begin{center}%
%		{\fontsize{14pt}{\baselineskip}\selectfont
%			\textbf{DOKUZ EYLÜL UNIVERSITY \\
%				GRADUATE SCHOOL OF NATURAL AND APPLIED SCIENCES}
%			\vspace{3\baselineskip}
%			}% aşağıdaki boş satırı silme. 
%			
%		{\fontsize{18pt}{\baselineskip}\selectfont
%			\textbf{\MakeUppercase{\@titleEN}}\par
%			\vspace{3\baselineskip}}
%			
%		{\fontsize{14pt}{\baselineskip}\selectfont
%			\textbf{by}\\
%			\textbf{\@author}}%
%		\vfill
%		%\vskip 1.5em%
%		
%		{\fontsize{12pt}{\baselineskip}\selectfont 		\textbf{\@dateEN}\\ \textbf{İZMİR}}
%	\end{center}

% Türkçe ve İngilizce iç kapak 
	\clearpage
	\thispagestyle{empty}
	\begin{center}
		{\fontsize{18pt}{\baselineskip}\selectfont
			\textbf{\MakeUppercase{\@title}}\par
			\vspace{3\baselineskip}}%
	
		{\fontsize{12pt}{0.6\baselineskip}\selectfont
			\textbf{Dokuz Eylül Üniversitesi Fen Bilimleri Enstitüsü \\
				Yüksek Lisans Tezi\\
				Bilgisayar Bilimleri Ana Bilim Dalı}\par
			\vspace{7.2\baselineskip}}

		{\fontsize{14pt}{\baselineskip}\selectfont
			\textbf{\@author}}%
		\vfill
		%\vskip 1.5em%
		
		{\fontsize{12pt}{\baselineskip}\selectfont \textbf{\@date}\\ \textbf{İZMİR}}%    
	\end{center}

%	\clearpage
%	\thispagestyle{empty}
%	\begin{center}
%		{\fontsize{18pt}{\baselineskip}\selectfont
%			\textbf{\MakeUppercase{\@titleEN}}\par
%			\vspace{3\baselineskip}}%
%				
%		{\fontsize{12pt}{0.6\baselineskip}\selectfont
%			\textbf{A Thesis Submitted to the \\
%				Graduate School of Natural and Applied Sciences of Dokuz Eylül University\\
%				In Partial Fulfillment of the Requirements for the Degree of Master of
%				Computer Science}}\par
%			\vspace{3.6\baselineskip}
%				
%		{\fontsize{14pt}{\baselineskip}\selectfont
%					\textbf{by}\\
%					\textbf{\@author}}%
%		\vfill
%		
%		{\fontsize{12pt}{\baselineskip}\selectfont 
%			\textbf{\@date}\\  \textbf{İZMİR}}%       
%	\end{center}
%		
%	\clearpage
%	\thispagestyle{empty}
  \end{titlepage}

  \setcounter{footnote}{0}%
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@date\@empty
  \global\let\title\relax
  \global\let\titleEN\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\dateEN\relax
  \global\let\and\relax
  \pagenumbering{roman}
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Ön Bölüm sayfaları, küçük roma rakamları (i,ii, iii,...) ile numaralandırılmalıdır.
  \setcounter{page}{2}
  \resultformTR
  %\resultformEN
}
\fi

\def \dotdot{\protect\leaders\hbox{$\m@th
		\mkern \@dotsep mu\hbox{.}\mkern \@dotsep
		mu$}\hfill	
}
\newcommand*\chaptermark[1]{}
\setcounter{secnumdepth}{6}
\newcounter {chapter}
\newcounter {section}[chapter]
\newcounter {subsection}[section]
\newcounter {subsubsection}[subsection]
\newcounter {paragraph}[subsubsection]
\newcounter {subparagraph}[paragraph]

\renewcommand\thechapter {\@arabic\c@chapter}
\renewcommand\thesection {\thechapter.\@arabic\c@section}
\renewcommand\thesubsection   {\thesection.\@arabic\c@subsection}
\renewcommand\thesubsubsection{\thesubsection.\@arabic\c@subsubsection}
\renewcommand\theparagraph    {\thesubsubsection.\@arabic\c@paragraph}
\renewcommand\thesubparagraph {\theparagraph.\@arabic\c@subparagraph}
\newcommand\@chapapp{\chaptername}

\newcommand*\l@part[2]{%
  \ifnum \c@tocdepth >-2\relax
  \vspace{-41\p@}
    \addpenalty{-\@highpenalty}%
    \addvspace{2.25em \@plus\p@}%
    \setlength\@tempdima{3em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      {\leavevmode
       #1\dotdot #2}\par
       \nobreak
         \global\@nobreaktrue
         \everypar{\global\@nobreakfalse\everypar{}}%
    \endgroup
  \fi}

\newcommand\chapter{\clearpage
                    \thispagestyle{plain}%
                    \global\@topnum\z@
                    \@afterindentfalse
                    \secdef\@chapter\@schapter}
\def\@chapter[#1]#2{\ifnum \c@secnumdepth >\m@ne
                         \refstepcounter{chapter}%
                         \typeout{\@chapapp\space\thechapter.}%
                         \addcontentsline{toc}{chapter}%
                         {\protect\uppercase{\@chapapp}\ \NUMBERstring{chapter} \textendash\ \uppercase{#1}\dotdot\nobreak
                         \rightskip \@tocrmarg \parfillskip -\rightskip
                         }%
                    \else
                      \addcontentsline{toc}{chapter}{#1}%
                    \fi
                    \chaptermark{#1}%
                    \if@twocolumn
                      \@topnewpage[\@makechapterhead{#2}]%
                    \else
                      \@makechapterhead{#2}%
                      \@afterheading
                    \fi}
\def\@makechapterhead#1{%
  {\parindent \z@ \raggedright \normalfont
    \ifnum \c@secnumdepth >\m@ne
		\centering
        \bfseries \@chapapp\space \NUMBERstring{chapter}
		\par\nobreak
		\vspace{-15\p@}
    \fi
    \interlinepenalty\@M
    \bfseries  \MakeUppercase{#1}\par\nobreak
    \vspace{8\p@}
  }}
\def\@schapter#1{
\@makeschapterhead{#1}%
}
\def\@makeschapterhead#1{%
  {\parindent \z@ \raggedright
    \normalfont
    \interlinepenalty\@M
     \bfseries  #1\par\nobreak
     \vspace{-12\p@} 
  }}

\def\@hangfrom#1{\setbox\@tempboxa\hbox{{#1}}%
      \hangindent 0pt%\wd\@tempboxa
      \noindent\box\@tempboxa}
      
\def\@seccntformat#1{\csname the#1\endcsname\ }
\newcommand\section{\@startsection {section}{1}{\z@}%
                                   {-3.5ex \@plus -1ex \@minus -.2ex}%
                                   {2.3ex \@plus.2ex}%
                                   {\normalfont\bfseries}}
\newcommand\subsection{\@startsection{subsection}{2}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont\itshape\bfseries}}
\newcommand\subsubsection{\@startsection{subsubsection}{3}{.5cm}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont\itshape}}
\newcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
                                    {3.25ex \@plus1ex \@minus.2ex}%
                                    {-1em}%
                                    {\normalfont\normalsize\bfseries}}
\newcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
                                       {3.25ex \@plus1ex \@minus .2ex}%
                                       {-1em}%
                                      {\normalfont\normalsize\bfseries}}
\if@twocolumn
  \setlength\leftmargini  {2em}
\else
  \setlength\leftmargini  {2.5em}
\fi
\leftmargin\leftmargini
\setlength\leftmarginii  {2.2em}
\setlength\leftmarginiii {1.87em}
\setlength\leftmarginiv  {1.7em}
\if@twocolumn
  \setlength\leftmarginv  {.5em}
  \setlength\leftmarginvi {.5em}
\else
  \setlength\leftmarginv  {1em}
  \setlength\leftmarginvi {1em}
\fi
\setlength  \labelsep  {.5em}
\setlength  \labelwidth{\leftmargini}
\addtolength\labelwidth{-\labelsep}
\@beginparpenalty -\@lowpenalty
\@endparpenalty   -\@lowpenalty
\@itempenalty     -\@lowpenalty
\renewcommand\theenumi{\@arabic\c@enumi}
\renewcommand\theenumii{\@alph\c@enumii}
\renewcommand\theenumiii{\@roman\c@enumiii}
\renewcommand\theenumiv{\@Alph\c@enumiv}
\newcommand\labelenumi{\theenumi.}
\newcommand\labelenumii{(\theenumii)}
\newcommand\labelenumiii{\theenumiii.}
\newcommand\labelenumiv{\theenumiv.}
\renewcommand\p@enumii{\theenumi}
\renewcommand\p@enumiii{\theenumi(\theenumii)}
\renewcommand\p@enumiv{\p@enumiii\theenumiii}
\newcommand\labelitemi{\textbullet}
\newcommand\labelitemii{\normalfont\bfseries \textendash}
\newcommand\labelitemiii{\textasteriskcentered}
\newcommand\labelitemiv{\textperiodcentered}
\newenvironment{description}
               {\list{}{\labelwidth\z@ \itemindent-\leftmargin
                        \let\makelabel\descriptionlabel}}
               {\endlist}
\newcommand*\descriptionlabel[1]{\hspace\labelsep
                                \normalfont\bfseries #1}
\if@titlepage
  \newenvironment{abstract}{%
      \titlepage
      \null\vfil
      \@beginparpenalty\@lowpenalty
      \begin{center}%
        \bfseries \abstractname
        \@endparpenalty\@M
      \end{center}}%
     {\par\vfil\null\endtitlepage}
\else
  \newenvironment{abstract}{%
      \if@twocolumn
        \section*{\abstractname}%
      \else
        \small
        \begin{center}%
          {\bfseries \abstractname\vspace{-.5em}\vspace{\z@}}%
        \end{center}%
        \quotation
      \fi}
      {\if@twocolumn\else\endquotation\fi}
\fi
\newenvironment{verse}
               {\let\\\@centercr
                \list{}{\itemsep      \z@
                        \itemindent   -1.5em%
                        \listparindent\itemindent
                        \rightmargin  \leftmargin
                        \advance\leftmargin 1.5em}%
                \item\relax}
               {\endlist}
\newenvironment{quotation}
               {\list{}{\listparindent 1.5em%
                        \itemindent    \listparindent
                        \rightmargin   \leftmargin
                        \parsep        \z@ \@plus\p@}%
                \item\relax}
               {\endlist}
\newenvironment{quote}
               {\list{}{\rightmargin\leftmargin}%
                \item\relax}
               {\endlist}
\if@compatibility
\newenvironment{titlepage}
    {%
      \if@twocolumn
        \@restonecoltrue\onecolumn
      \else
        \@restonecolfalse\newpage
      \fi
      \thispagestyle{empty}%
      \setcounter{page}\z@
    }%
    {\if@restonecol\twocolumn \else \newpage \fi
    }
\else
\newenvironment{titlepage}
    {%
      \if@twocolumn
        \@restonecoltrue\onecolumn
      \else
        \@restonecolfalse\newpage
      \fi
      \thispagestyle{empty}%
      \setcounter{page}\@ne
    }%
    {\if@restonecol\twocolumn \else \newpage \fi
     \if@twoside\else
        \setcounter{page}\@ne
     \fi
    }
\fi
\newcommand\appendix{\par
  \setcounter{chapter}{0}%
  \setcounter{section}{0}%
  \gdef\@chapapp{\appendixname}%
  \gdef\thechapter{\@Alph\c@chapter}}
\setlength\arraycolsep{5\p@}
\setlength\tabcolsep{6\p@}
\setlength\arrayrulewidth{.4\p@}
\setlength\doublerulesep{2\p@}
\setlength\tabbingsep{\labelsep}
\skip\@mpfootins = \skip\footins
\setlength\fboxsep{3\p@}
\setlength\fboxrule{.4\p@}
\@addtoreset {equation}{chapter}
\renewcommand\theequation
  {\ifnum \c@chapter>\z@ \thechapter.\fi \@arabic\c@equation}
\newcounter{figure}[chapter]
\renewcommand \thefigure
     {\ifnum \c@chapter>\z@ Şekil~\thechapter.\fi \@arabic\c@figure}
\def\fps@figure{tbp}
\def\ftype@figure{1}
\def\ext@figure{lof}
\def\fnum@figure{\thefigure}
\newenvironment{figure}
               {\@float{figure}}
               {\end@float}
\newenvironment{figure*}
               {\@dblfloat{figure}}
               {\end@dblfloat}
\newcounter{table}[chapter]
\renewcommand \thetable
     {\ifnum \c@chapter>\z@ Tablo~\thechapter.\fi \@arabic\c@table}
\def\fps@table{tbp}
\def\ftype@table{2}
\def\ext@table{lot}
\def\fnum@table{\thetable}
\newenvironment{table}
               {\@float{table}}
               {\end@float}
\newenvironment{table*}
               {\@dblfloat{table}}
               {\end@dblfloat}
\newlength\abovecaptionskip
\newlength\belowcaptionskip
\setlength\abovecaptionskip{10\p@}
\setlength\belowcaptionskip{0\p@}
\long\def\@makecaption#1#2{%
  \vskip\abovecaptionskip
  \sbox\@tempboxa{#1 #2}%
  \ifdim \wd\@tempboxa >\hsize
    #1 #2\par
  \else
    \global \@minipagefalse
    \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
  \fi
  \vskip\belowcaptionskip}
\DeclareOldFontCommand{\rm}{\normalfont\rmfamily}{\mathrm}
\DeclareOldFontCommand{\sf}{\normalfont\sffamily}{\mathsf}
\DeclareOldFontCommand{\tt}{\normalfont\ttfamily}{\mathtt}
\DeclareOldFontCommand{\bf}{\normalfont\bfseries}{\mathbf}
\DeclareOldFontCommand{\it}{\normalfont\itshape}{\mathit}
\DeclareOldFontCommand{\sl}{\normalfont\slshape}{\@nomath\sl}
\DeclareOldFontCommand{\sc}{\normalfont\scshape}{\@nomath\sc}
\DeclareRobustCommand*\cal{\@fontswitch\relax\mathcal}
\DeclareRobustCommand*\mit{\@fontswitch\relax\mathnormal}
\newcommand\@pnumwidth{1.55em}
\newcommand\@tocrmarg{0em}
\newcommand\@dotsep{.2}

\setcounter{tocdepth}{6}

% Table of Contents
%
\newcommand\tableofcontents{%
    \chapter*{\contentsname}
        \@mkboth{\contentsname}{\contentsname}
    \vspace{.7cm}
    {\hfill \bfseries Sayfa\par}     
    \@starttoc{toc}%
    }

\newcommand*\l@chapter[2]{%
  \ifnum \c@tocdepth >\m@ne
    \addpenalty{-\@highpenalty}%
    \vskip 1.0em \@plus\p@
    \setlength\@tempdima{1.5em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      \leavevmode \bfseries
      \hskip -\leftskip
      #1\nobreak\hfil \nobreak #2 \par
      \penalty\@highpenalty
    \endgroup
  \fi}
\def\@dottedtocline#1#2#3#4#5{%
	\ifnum #1>\c@tocdepth \else
	\vskip \z@ \@plus.2\p@
	{\leftskip #2\relax \rightskip \@tocrmarg \parfillskip -\rightskip
		\parindent #2\relax\@afterindenttrue
		\interlinepenalty\@M
		\leavevmode
		\@tempdima #3\relax
		\null\nobreak\hskip -\leftskip
		{#4}\nobreak\dotdot\nobreak
		{\normalfont \normalcolor #5}%
		\par}%
	\vspace{-15\p@}
	\fi}
	
\def\@dottedtoclineFT#1#2#3#4#5{%
	\ifnum #1>\c@tocdepth \else
	\vskip \z@ \@plus.2\p@
	{\leftskip #2\relax \rightskip \@tocrmarg \parfillskip -\rightskip
		\parindent #2\relax\@afterindenttrue
		\interlinepenalty\@M
		\leavevmode
		\@tempdima #3\relax
		\advance\leftskip \@tempdima \null\nobreak\hskip -\leftskip
		{#4}\nobreak\dotdot\nobreak
		{\normalfont \normalcolor #5}%
		\par}%
	\vspace{-15\p@}
	\fi
}

\newcommand*\l@section{\@dottedtocline{1}{1.5em}{1.5em}}  
\newcommand*\l@subsection{\@dottedtocline{2}{3em}{2.3em}}
\newcommand*\l@subsubsection{\@dottedtocline{3}{4.5em}{3em}}
\newcommand*\l@paragraph{\@dottedtocline{4}{10em}{5em}}
\newcommand*\l@subparagraph{\@dottedtocline{5}{12em}{6em}}
% List of Figures
%
\newcommand\listoffigures{%
    \chapter*{\listfigurename}%
      \@mkboth{\MakeUppercase\listfigurename}%
      {\MakeUppercase\listfigurename}%
      \vspace{.6cm}
      {\hfill \bfseries Sayfa\par}\vspace{-12\p@} \addcontentsline{toc}{part}{\MakeUppercase{\listfigurename}}
    \@starttoc{lof}%
    }
\newcommand*\l@figure{\@dottedtoclineFT{1}{0em}{4.0em}}
% List of Tables
%
\newcommand\listoftables{%
    \chapter*{\listtablename}%
      \@mkboth{\MakeUppercase\listtablename}%
      {\MakeUppercase\listtablename}%
      \vspace{.7cm}
        	  {\hfill \bfseries Sayfa\par}\vspace{-12\p@}
              \addcontentsline{toc}{part}{\MakeUppercase{\listtablename}}
    \@starttoc{lot}%
    %\clearpage
    %\pagenumbering{arabic}
    }
\newcommand*\l@table{\@dottedtoclineFT{1}{0em}{4.0em}}
\newdimen\bibindent
\setlength\bibindent{1.5em}
\newenvironment{thebibliography}[1]
     {\chapter*{\bibname}%
      \@mkboth{\MakeUppercase\bibname}{\MakeUppercase\bibname}%
      \list{\@biblabel{\@arabic\c@enumiv}}%
           {\settowidth\labelwidth{\@biblabel{#1}}%
            \leftmargin\labelwidth
            \advance\leftmargin\labelsep
            \@openbib@code
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \sloppy
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\def\@noitemerr
       {\@latex@warning{Empty `thebibliography' environment}}%
      \endlist}
\newcommand\newblock{\hskip .11em\@plus.33em\@minus.07em}
\let\@openbib@code\@empty
\newenvironment{theindex}
               {\if@twocolumn
                  \@restonecolfalse
                \else
                  \@restonecoltrue
                \fi
                \twocolumn[\@makeschapterhead{\indexname}]%
                \@mkboth{\MakeUppercase\indexname}%
                        {\MakeUppercase\indexname}%
                \thispagestyle{plain}\parindent\z@
                \parskip\z@ \@plus .3\p@\relax
                \columnseprule \z@
                \columnsep 35\p@
                \let\item\@idxitem}
               {\if@restonecol\onecolumn\else\clearpage\fi}
\newcommand\@idxitem{\par\hangindent 40\p@}
\newcommand\subitem{\@idxitem \hspace*{20\p@}}
\newcommand\subsubitem{\@idxitem \hspace*{30\p@}}
\newcommand\indexspace{\par \vskip 10\p@ \@plus5\p@ \@minus3\p@\relax}
\renewcommand\footnoterule{%
  \kern-3\p@
  \hrule\@width.4\columnwidth
  \kern2.6\p@}
\@addtoreset{footnote}{chapter}
\newcommand\@makefntext[1]{%
    \parindent 1em%
    \noindent
    \hb@xt@1.8em{\hss\@makefnmark}#1}

\newcommand\contentsname{İÇİNDEKİLER}
\newcommand\listfigurename{ŞEKİLLER LİSTESİ}
\newcommand\listtablename{TABLOLAR LİSTESİ}
\newcommand\bibname{KAYNAKLAR}
\newcommand\indexname{Index}
\newcommand\figurename{Şekil}
\newcommand\tablename{Tablo}
\newcommand\partname{Part}
\newcommand\chaptername{BÖLÜM}
\newcommand\appendixname{EKLER}
\newcommand\abstractname{Abstract}

\def\today{\ifcase\month\or
  January\or February\or March\or April\or May\or June\or
  July\or August\or September\or October\or November\or December\fi
  \space\number\day, \number\year}
\setlength\columnsep{10\p@}
\setlength\columnseprule{0\p@}
\pagestyle{plain}
\pagenumbering{arabic}
\if@twoside
\else
  \raggedbottom
\fi
\if@twocolumn
  \twocolumn
  \sloppy
  \flushbottom
\else
  \onecolumn
\fi


\def\titleEN#1{\gdef\@titleEN{#1}}
\def\dateEN#1{\gdef\@dateEN{#1}}
\def\supervisor#1{\gdef\@supervisor{#1}}
\def\cosupervisor#1{\gdef\@cosupervisor{#1}}

\def\comiteememberone#1{\gdef\@comiteememberone{#1}}
\def\comiteemembertwo#1{\gdef\@comiteemembertwo{#1}}
\def\comiteememberthree#1{\gdef\@comiteememberthree{#1}}

\def\examiningcomiteememberone#1{\gdef\@examiningcomiteememberone{#1}}
\def\examiningcomiteemembertwo#1{\gdef\@examiningcomiteemembertwo{#1}}
\def\director#1{\gdef\@director{#1}}
\newcommand \Dotdot {\leavevmode \cleaders \hb@xt@ .18em{\hss .\hss }\hfill \kern \z@}

\def \resultformTR{
	\clearpage
	
	\chapter*{\center 
		YÜKSEK LİSANS TEZİ SINAV SONUÇ FORMU}
	\addcontentsline{toc}{part}{\MakeUppercase{SINAV SONUÇ FORMU}}
	\vskip0.2in
	\noindent \textbf{\MakeUppercase{\@author},} tarafından \textbf{\MakeUppercase{\@supervisor}} yönetiminde hazırlanan \textquotedblleft\textbf{\MakeUppercase{\@title}}\textquotedblright\ başlıklı tez tarafımızdan okunmuş, kapsamı ve niteliği açısından bir Yüksek Lisans tezi olarak kabul edilmiştir.
	
\vspace{0.4cm}
\vfill
	\begin{center}
	\makebox[.42\textwidth]{\Dotdot}\\[2ex]
		\begin{tabular}{c}
			\@supervisor\\
			\\[-2ex]	\hline \\
			Danışman\\
			\null\hfill\rule{.38\textwidth}{0pt}\hfill\null
		\end{tabular}
	\end{center}
	\vspace{0.5cm}
	\vfill
	\noindent
	\begin{minipage}{.42\textwidth}
		\centering
		\makebox[.95\textwidth]{\Dotdot}\\[1ex]
		\begin{tabular}{c}
			\@comiteememberone\\
				\hline\\ [-1.5ex]
			Jüri Üyesi\\
			\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
		\end{tabular}
	\end{minipage}
	\hspace{.1\textwidth}
	\begin{minipage}{.42\textwidth}
		\centering
		\makebox[.95\textwidth]{\Dotdot}\\[1ex]
		\begin{tabular}{c}
			\@comiteemembertwo\\
				\hline\\ [-1.5ex]
			Jüri Üyesi\\
			\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
		\end{tabular}
	\end{minipage}
	\hspace{.5\textwidth}
	\begin{center}
		\begin{minipage}{.42\textwidth}
			\centering
			\makebox[.95\textwidth]{\Dotdot}\\[1ex]
			\begin{tabular}{c}
				\@comiteememberthree\\
					\hline\\ [-1.5ex]
				Jüri Üyesi\\
				\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
			\end{tabular}
		\end{minipage}
	\end{center}

	\vspace{0.7cm}
	\vfill
	\noindent
	\vfill
	\begin{center}
		\begin{tabular}{c}
			\null \\
			\hline\\[-3ex]
			\mbox{\@director} \\[-1ex]
			Müdür\\[-1ex]
			Fen Bilimleri Enstitüsü\\
		\end{tabular}
	\end{center}
}
\def \resultformEN{
	\clearpage
	
	\chapter*{\center 
		M.Sc. THESIS EXAMINATION RESULT FORM}
	\addcontentsline{toc}{part}{\MakeUppercase{THESIS EXAMINATION RESULT FORM}}
	\vskip0.2in
	\noindent We have read the thesis entitled \textquotedblleft\textbf{\MakeUppercase{\@titleEN}}\textquotedblright\ completed by \textbf{\MakeUppercase{\@author}} under supervision of \textbf{\MakeUppercase{\@supervisor}} 
	and we certify that in our opinion it is fully adequate, in scope and in quality, as a thesis for the degree of Master of Science.
	
\vspace{0.4cm}
	\vfill
	\begin{center}
		\makebox[.42\textwidth]{\Dotdot}\\[2ex]
		\begin{tabular}{c}
			\@supervisor\\
			\hline\\[-2ex]
			Supervisor\\
			\null\hfill\rule{.38\textwidth}{0pt}\hfill\null
		\end{tabular}
	\end{center}
	
\vspace{0.7cm}
	\vfill
	\noindent
	\begin{minipage}{.44\textwidth}
	\centering
	\makebox[.95\textwidth]{\Dotdot}\\[1ex]
		\begin{tabular}{c}
			\@comiteememberone\\
			\hline\\[-2ex]
			Jury Member\\
			\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
		\end{tabular}
	\end{minipage}	
	\hspace{.1\textwidth}
	\begin{minipage}{.44\textwidth}
	\centering
	\makebox[.95\textwidth]{\Dotdot}\\[1ex]
		\begin{tabular}{c}
			\@comiteemembertwo\\
			\hline\\[-2ex]
			Jury Member\\
			\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
		\end{tabular}
	\end{minipage}

\vspace{0.7cm}
\vfill
\noindent

\vfill
\begin{center}
	\begin{tabular}{c}
		\null \\
		\hline\\[-3ex]
		\mbox{\@director} \\%[-1ex]
		Director \\%[-1ex]
		Graduate School of Natural and Applied Sciences\\
	\end{tabular}
\end{center}
}

\newenvironment{acknowledgment}
  {	
  	\clearpage
  	\chapter*{\center TEŞEKKÜR}
  	\addcontentsline{toc}{part}{TEŞEKKÜR}
  	\vskip0.05in
  }
  {
  	\begin{flushright}
  		\@author
  	\end{flushright}
  }

\newenvironment{abstractTR}
{	
	\clearpage
	\chapter*{\center \@title \vskip0.05in ÖZ}
	\addcontentsline{toc}{part}{ÖZ}	
	\vskip0.2in}
{
	
}

\newenvironment{abstractEN}
{	
	\clearpage
	\chapter*{\center \@titleEN \vskip0.05in ABSTRACT}
	\addcontentsline{toc}{part}{ABSTRACT}	
	\vskip0.2in}
{
	
}
\def\startappendix{
	\clearpage
	\addcontentsline{toc}{chapter}{\protect\MakeUppercase{\appendixname}\dotdot\nobreak}	
	\appendix
	\chapter*{\appendixname}
	\setcounter{section}{0}%
	\setcounter{figure}{0}%
	\setcounter{table}{0}%
	\setcounter{equation}{0}
	\renewcommand{\theequation}{EK \arabic{equation}}
	\renewcommand{\thesection}{EK \arabic{section}}
	\renewcommand{\thetable}{Tablo~A.\@arabic\c@table}
	\renewcommand{\thefigure}{Şekil~A.\@arabic\c@figure}
}

\def \addappendixpart#1{
	\addtocounter{section}{1}
	\addcontentsline{toc}{section}{\thesection :\ #1\protect\leaders\hbox{$\m@th
				\mkern\@dotsep mu\hbox{.}\mkern\@dotsep
				mu$}\hfil}
	\section*{\thesection\protect :\ #1}
}

\def\startbibliography{
		\clearpage
		\addcontentsline{toc}{chapter}{\protect\MakeUppercase{\bibname}\dotdot\nobreak}
		\bibliographystyle{fbe}
	}

\tolerance=1
\emergencystretch=\maxdimen
\hyphenpenalty=10000
\hbadness=10000

\usepackage[comma,authoryear]{natbib}
\usepackage{caption}
\captionsetup[table]{singlelinecheck=false,justification=justified,font=footnotesize,labelsep=space}
\captionsetup[figure]{singlelinecheck=true,font=footnotesize,labelsep=space}

\endinput