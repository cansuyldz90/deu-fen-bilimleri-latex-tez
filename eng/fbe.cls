%%
%% This is file `report.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% classes.dtx  (with options: `report')
%% 
%% This is a generated file.
%% 
%% Copyright 1993 1994 1995 1996 1997 1998 1999 2000 2001 2002 2003 2004 2005 2006 2007 2008 2009
%% The LaTeX3 Project and any individual authors listed elsewhere
%% in this file.
%% 
%% This file was generated from file(s) of the LaTeX base system.
%% --------------------------------------------------------------
%% 
%% It may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3c
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%% 
%% This file has the LPPL maintenance status "maintained".
%% 
%% This file may only be distributed together with a copy of the LaTeX
%% base system. You may however distribute the LaTeX base system without
%% such generated files.
%% 
%% The list of all files belonging to the LaTeX base distribution is
%% given in the file `manifest.txt'. See also `legal.txt' for additional
%% information.
%% 
%% The list of derived (unpacked)\tableofcontents files belonging to the distribution
%% and covered by LPPL is defined by the unpacking scripts (with
%% extension .ins) which are part of the distribution.
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{fbe}
              [2015/11/25 v1.0h
 Standard LaTeX document class]
\usepackage{amssymb}
\RequirePackage{fmtcount}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{setspace}

% 16.03.2024 -----------------------------------
\RequirePackage{xkeyval}
\def\signpagevspacewithcoadviser{0.7cm}
\define@key{fbe.cls}{imzalarArasiBosluk}[0.7cm]{
	\def\signpagevspacewithcoadviser{#1}
}
\ExecuteOptionsX{imzalarArasiBosluk}
\ProcessOptionsX
% -----------------------------------------------
 
\newcommand\@ptsize{} 
\newif\if@restonecol
\newif\if@titlepage
\@titlepagetrue
\newif\if@openright
\newif\if@PhD
\newif\if@withcoadvisor
\newif\if@coadvisorsigned
\@withcoadvisorfalse
\@coadvisorsignedfalse
\@PhDtrue

\DeclareOption{a4paper}
   {\setlength\paperheight {297mm}%
    \setlength\paperwidth  {210mm}}

\DeclareOption{12pt}{\renewcommand\@ptsize{2}}

\DeclareOption{oneside}{\@twosidefalse \@mparswitchfalse}

\DeclareOption{final}{\setlength\overfullrule{0pt}}

\DeclareOption{titlepage}{\@titlepagetrue}

\DeclareOption{openany}{\@openrightfalse}

\DeclareOption{onecolumn}{\@twocolumnfalse}

\DeclareOption{msc}{\@PhDfalse}

\DeclareOption{withcoadvisor}{\@withcoadvisortrue}

\DeclareOption{coadvisorsigned}{\@coadvisorsignedtrue}

\ExecuteOptions{a4paper,12pt,oneside,onecolumn,final,openany} % default options for the document.
\ProcessOptions
\input{size1\@ptsize.clo}
%\newcommand{\signpagevspacewithcoadviser}{0.7cm}
\setlength\voffset{-1in} % https://en.wikibooks.org/wiki/LaTeX/Page_Layout
\setlength\hoffset{-1in}
\setlength\marginparwidth{0mm}
\setlength\oddsidemargin{40mm}
\setlength\evensidemargin{40mm}
\setlength\marginparsep{0mm}
\setlength\textwidth {145mm}
\setlength\topmargin{30mm}
\setlength\headheight{0mm}
\setlength\headsep{0mm}
\setlength\textheight {237mm}
\setlength\footskip{13mm} 
\setlength\parindent{5mm}
\setlength\parskip {\baselineskip}
\renewcommand{\baselinestretch}{1.5}

\renewcommand{\@afterindentfalse}{\@afterindenttrue}
\if@twoside
  \def\ps@headings{%
      \let\@oddfoot\@empty\let\@evenfoot\@empty
      \def\@evenhead{\thepage\hfil\slshape\leftmark}%
      \def\@oddhead{{\slshape\rightmark}\hfil\thepage}%
      \let\@mkboth\markboth
    \def\chaptermark##1{%
      \markboth {\MakeUppercase{%
        \ifnum \c@secnumdepth >\m@ne
            \@chapapp\ \thechapter. \ %
        \fi
        ##1}}{}}%
    \def\sectionmark##1{%
      \markright {\MakeUppercase{%
        \ifnum \c@
         >\z@
          \thesection. \ %
        \fi
        ##1}}}}
\else
  \def\ps@headings{%
    \let\@oddfoot\@empty
    \def\@oddhead{{\slshape\rightmark}\hfil\thepage}%
    \let\@mkboth\markboth
    \def\chaptermark##1{%
      \markright {\MakeUppercase{%
        \ifnum \c@secnumdepth >\m@ne
            \@chapapp\ \thechapter. \ %
        \fi
        ##1}}}}
\fi
\def\ps@myheadings{%
    \let\@oddfoot\@empty\let\@evenfoot\@empty
    \def\@evenhead{\thepage\hfil\slshape\leftmark}%
    \def\@oddhead{{\slshape\rightmark}\hfil\thepage}%
    \let\@mkboth\@gobbletwo
    \let\chaptermark\@gobble
    \let\sectionmark\@gobble
    }
    
\if@titlepage  
\newcommand\maketitle{
	\begin{titlepage}
	  \let\footnotesize\small
	  \let\footnoterule\relax
	  \let \footnote \thanks
	  \begin{center}
	  	{\fontsize{14pt}{\baselineskip}\selectfont
	  		\textbf{DOKUZ EYLÜL UNIVERSITY \\
	  			\hspace{-1mm}GRADUATE SCHOOL OF NATURAL AND APPLIED SCIENCES}
	  		\vspace{3.3\baselineskip}
	  		}% aşağıdaki boş satırı silme. 
	  		
	    {\fontsize{18pt}{\baselineskip}\selectfont
	    	\textbf{\MakeUppercase{\@title}}\par
	    	\vspace{3\baselineskip}
	    	}
	    	
	    {\fontsize{14pt}{\baselineskip}\selectfont
	      	\textbf{by}\\
	        \textbf{\@author}
	      }
	      \vfill
	    {\fontsize{12pt}{\baselineskip}\selectfont \textbf{\@date}\\ \textbf{İZMİR}}       % Set date in \large size.
	  \end{center}
		\clearpage
		\thispagestyle{empty}
		\if@PhD
				\begin{center}
					{\fontsize{18pt}{\baselineskip}\selectfont
						\textbf{\MakeUppercase{\@title}}\par
						\vspace{3.6\baselineskip}
					}
					
					{\fontsize{12pt}{.61\baselineskip}\selectfont
						\textbf{A Thesis Submitted to the\\
							Graduate School of Natural And Applied Sciences of Dokuz Eylül University\\
							In Partial Fulfillment of the Requirements for the Degree of Doctor of\\
							Philosophy in Mathematics}\par
						\vspace{5.4\baselineskip}
					}
					{\fontsize{14pt}{\baselineskip}\selectfont
						\textbf{by}\\
						\textbf{\@author}
					}
					\vfill
					%\vskip 1.5em
					{\fontsize{12pt}{\baselineskip}\selectfont \textbf{\@date}\\ \textbf{İZMİR}}       % Set date in \large size.
				\end{center}
		\else
				\begin{center}
					{\fontsize{18pt}{\baselineskip}\selectfont
						\textbf{\MakeUppercase{\@title}}\par
						\vspace{3.6\baselineskip}
					}
					
					{\fontsize{12pt}{.61\baselineskip}\selectfont
						\textbf{A Thesis Submitted to the\\
							Graduate School of Natural And Applied Sciences of Dokuz Eylül University\\
							In Partial Fulfillment of the Requirements for the Degree of Master of\\
							Science in Mathematics}\par
						\vspace{5.4\baselineskip}
					}
					{\fontsize{14pt}{\baselineskip}\selectfont
						\textbf{by}\\
						\textbf{\@author}
					}
					\vfill
					%\vskip 1.5em
					{\fontsize{12pt}{\baselineskip}\selectfont \textbf{\@date}\\ \textbf{İZMİR}}       % Set date in \large size.
				\end{center}
		\fi
	\end{titlepage}
  \setcounter{footnote}{0}
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@date\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
  \pagenumbering{roman}
  \setcounter{page}{2}
  \resultform
}
\fi
\def \dotdot{\protect\leaders\hbox{$\m@th
		\mkern \@dotsep mu\hbox{.}\mkern \@dotsep
		mu$}\hfill	
}
\newcommand*\chaptermark[1]{}
\setcounter{secnumdepth}{6}
\newcounter {chapter}
\newcounter {section}[chapter]
\newcounter {subsection}[section]
\newcounter {subsubsection}[subsection]
\newcounter {paragraph}[subsubsection]
\newcounter {subparagraph}[paragraph]

\renewcommand \thechapter {\@arabic\c@chapter}
\renewcommand \thesection {\thechapter.\@arabic\c@section}
\renewcommand\thesubsection   {\thesection.\@arabic\c@subsection}
\renewcommand\thesubsubsection{\thesubsection.\@arabic\c@subsubsection}
\renewcommand\theparagraph    {\thesubsubsection.\@arabic\c@paragraph}
\renewcommand\thesubparagraph {\theparagraph.\@arabic\c@subparagraph}
\newcommand\@chapapp{\chaptername}
\newcommand \theAsection {\thechapter.\@arabic\c@section}


\newcommand*\l@part[2]{%
  \ifnum \c@tocdepth >-2\relax
  \vspace{-41\p@}
    \addpenalty{-\@highpenalty}%
    \addvspace{2.25em \@plus\p@}%
    \setlength\@tempdima{3em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      {\leavevmode
       #1\dotdot #2}\par
       \nobreak
         \global\@nobreaktrue
         \everypar{\global\@nobreakfalse\everypar{}}%
    \endgroup
  \fi}

\newcommand\chapter{\clearpage
                    \thispagestyle{plain}%
                    \global\@topnum\z@
                    \@afterindentfalse
                    \secdef\@chapter\@schapter}
\def\@chapter[#1]#2{\ifnum \c@secnumdepth >\m@ne
                         \refstepcounter{chapter}%
                         \typeout{\@chapapp\space\thechapter.}%
                         \addcontentsline{toc}{chapter}%
                         {\protect\uppercase{\@chapapp}\  \NUMBERstring{chapter} %\arabic{chapter}
                         	 \textendash\ \uppercase{#1}\dotdot\nobreak
                         \rightskip \@tocrmarg \parfillskip -\rightskip
                         }%
                    \else
                      \addcontentsline{toc}{chapter}{#1}%
                    \fi
                    \chaptermark{#1}%
                    \if@twocolumn
                      \@topnewpage[\@makechapterhead{#2}]%
                    \else
                      \@makechapterhead{#2}%
                      \@afterheading
                    \fi}
\def\@makechapterhead#1{%
  {\parindent \z@ \raggedright \normalfont
    \ifnum \c@secnumdepth >\m@ne
		\centering
        \bfseries \@chapapp\space \NUMBERstring{chapter} %\arabic{chapter}
		\par\nobreak
		\vspace{-15\p@}
    \fi
    \interlinepenalty\@M
    \bfseries  \MakeUppercase{#1}\par\nobreak
    \vspace{8\p@}
  }}
\def\@schapter#1{
\@makeschapterhead{#1}%
}
\def\@makeschapterhead#1{%
  {\parindent \z@ \raggedright
    \normalfont
    \interlinepenalty\@M
     \bfseries  #1\par\nobreak
     \vspace{-12\p@} 
  }}

\def\@hangfrom#1{\setbox\@tempboxa\hbox{{#1}}%
      \hangindent 0pt%\wd\@tempboxa
      \noindent\box\@tempboxa}
      
\def\@seccntformat#1{\csname the#1\endcsname\ }
\newcommand\section{\@startsection {section}{1}{\z@}%
                                   {-3.5ex \@plus -1ex \@minus -.2ex}%
                                   {2.3ex \@plus.2ex}%
                                   {\normalfont\bfseries}}
\newcommand\subsection{\@startsection{subsection}{2}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont\itshape\bfseries}}
\newcommand\subsubsection{\@startsection{subsubsection}{3}{.5cm}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont\itshape}}
\newcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
                                    {3.25ex \@plus1ex \@minus.2ex}%
                                    {-1em}%
                                    {\normalfont\normalsize\bfseries}}
\newcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
                                       {3.25ex \@plus1ex \@minus .2ex}%
                                       {-1em}%
                                      {\normalfont\normalsize\bfseries}}
\if@twocolumn
  \setlength\leftmargini  {2em}
\else
  \setlength\leftmargini  {2.5em}
\fi
\leftmargin  \leftmargini
\setlength\leftmarginii  {2.2em}
\setlength\leftmarginiii {1.87em}
\setlength\leftmarginiv  {1.7em}
\if@twocolumn
  \setlength\leftmarginv  {.5em}
  \setlength\leftmarginvi {.5em}
\else
  \setlength\leftmarginv  {1em}
  \setlength\leftmarginvi {1em}
\fi
\setlength  \labelsep  {.5em}
\setlength  \labelwidth{\leftmargini}
\addtolength\labelwidth{-\labelsep}
\@beginparpenalty -\@lowpenalty
\@endparpenalty   -\@lowpenalty
\@itempenalty     -\@lowpenalty
\renewcommand\theenumi{\@arabic\c@enumi}
\renewcommand\theenumii{\@alph\c@enumii}
\renewcommand\theenumiii{\@roman\c@enumiii}
\renewcommand\theenumiv{\@Alph\c@enumiv}
\newcommand\labelenumi{\theenumi.}
\newcommand\labelenumii{(\theenumii)}
\newcommand\labelenumiii{\theenumiii.}
\newcommand\labelenumiv{\theenumiv.}
\renewcommand\p@enumii{\theenumi}
\renewcommand\p@enumiii{\theenumi(\theenumii)}
\renewcommand\p@enumiv{\p@enumiii\theenumiii}
\newcommand\labelitemi{\textbullet}
\newcommand\labelitemii{\normalfont\bfseries \textendash}
\newcommand\labelitemiii{\textasteriskcentered}
\newcommand\labelitemiv{\textperiodcentered}
\newenvironment{description}
               {\list{}{\labelwidth\z@ \itemindent-\leftmargin
                        \let\makelabel\descriptionlabel}}
               {\endlist}
\newcommand*\descriptionlabel[1]{\hspace\labelsep
                                \normalfont\bfseries #1}
\if@titlepage
  \newenvironment{abstract}{%
      \titlepage
      \null\vfil
      \@beginparpenalty\@lowpenalty
      \begin{center}%
        \bfseries \abstractname
        \@endparpenalty\@M
      \end{center}}%
     {\par\vfil\null\endtitlepage}
\else
  \newenvironment{abstract}{%
      \if@twocolumn
        \section*{\abstractname}%
      \else
        \small
        \begin{center}%
          {\bfseries \abstractname\vspace{-.5em}\vspace{\z@}}%
        \end{center}%
        \quotation
      \fi}
      {\if@twocolumn\else\endquotation\fi}
\fi
\newenvironment{verse}
               {\let\\\@centercr
                \list{}{\itemsep      \z@
                        \itemindent   -1.5em%
                        \listparindent\itemindent
                        \rightmargin  \leftmargin
                        \advance\leftmargin 1.5em}%
                \item\relax}
               {\endlist}
\newenvironment{quotation}
               {\list{}{\listparindent 1.5em%
                        \itemindent    \listparindent
                        \rightmargin   \leftmargin
                        \parsep        \z@ \@plus\p@}%
                \item\relax}
               {\endlist}
\newenvironment{quote}
               {\list{}{\rightmargin\leftmargin}%
                \item\relax}
               {\endlist}
\if@compatibility
\newenvironment{titlepage}
    {%
      \if@twocolumn
        \@restonecoltrue\onecolumn
      \else
        \@restonecolfalse\newpage
      \fi
      \thispagestyle{empty}%
      \setcounter{page}\z@
    }%
    {\if@restonecol\twocolumn \else \newpage \fi
    }
\else
\newenvironment{titlepage}
    {%
      \if@twocolumn
        \@restonecoltrue\onecolumn
      \else
        \@restonecolfalse\newpage
      \fi
      \thispagestyle{empty}%
      \setcounter{page}\@ne
    }%
    {\if@restonecol\twocolumn \else \newpage \fi
     \if@twoside\else
        \setcounter{page}\@ne
     \fi
    }
\fi
\newcommand\appendix{\par
  \setcounter{chapter}{0}%
  \setcounter{section}{0}%
  \gdef\@chapapp{\appendixname}%
  \gdef\thechapter{\@Alph\c@chapter}
  }
\setlength\arraycolsep{5\p@}
\setlength\tabcolsep{6\p@}
\setlength\arrayrulewidth{.4\p@}
\setlength\doublerulesep{2\p@}
\setlength\tabbingsep{\labelsep}
\skip\@mpfootins = \skip\footins
\setlength\fboxsep{3\p@}
\setlength\fboxrule{.4\p@}
\@addtoreset {equation}{chapter}
\renewcommand\theequation
  {\ifnum \c@chapter>\z@ \thechapter.\fi \@arabic\c@equation}
\newcounter{figure}[chapter]
\renewcommand \thefigure
     {\ifnum \c@chapter>\z@ Figure~\thechapter.\fi \@arabic\c@figure}
\def\fps@figure{tbp}
\def\ftype@figure{1}
\def\ext@figure{lof}
\def\fnum@figure{\thefigure}
\newenvironment{figure}
               {\@float{figure}}
               {\end@float}
\newenvironment{figure*}
               {\@dblfloat{figure}}
               {\end@dblfloat}
\newcounter{table}[chapter]
\renewcommand \thetable
     {\ifnum \c@chapter>\z@ Table~\thechapter.\fi \@arabic\c@table}
\def\fps@table{tbp}
\def\ftype@table{2}
\def\ext@table{lot}
\def\fnum@table{\thetable}
\newenvironment{table}
               {\@float{table}}
               {\end@float}
\newenvironment{table*}
               {\@dblfloat{table}}
               {\end@dblfloat}
\newlength\abovecaptionskip
\newlength\belowcaptionskip
\setlength\abovecaptionskip{10\p@}
\setlength\belowcaptionskip{0\p@}
\long\def\@makecaption#1#2{%
  \vskip\abovecaptionskip
  \sbox\@tempboxa{#1 #2}%
  \ifdim \wd\@tempboxa >\hsize
    #1 #2\par
  \else
    \global \@minipagefalse
    \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
  \fi
  \vskip\belowcaptionskip}
\DeclareOldFontCommand{\rm}{\normalfont\rmfamily}{\mathrm}
\DeclareOldFontCommand{\sf}{\normalfont\sffamily}{\mathsf}
\DeclareOldFontCommand{\tt}{\normalfont\ttfamily}{\mathtt}
\DeclareOldFontCommand{\bf}{\normalfont\bfseries}{\mathbf}
\DeclareOldFontCommand{\it}{\normalfont\itshape}{\mathit}
\DeclareOldFontCommand{\sl}{\normalfont\slshape}{\@nomath\sl}
\DeclareOldFontCommand{\sc}{\normalfont\scshape}{\@nomath\sc}
\DeclareRobustCommand*\cal{\@fontswitch\relax\mathcal}
\DeclareRobustCommand*\mit{\@fontswitch\relax\mathnormal}
\newcommand\@pnumwidth{1.55em}
\newcommand\@tocrmarg{0em}
\newcommand\@dotsep{.2}
\setcounter{tocdepth}{6}
\newcommand\tableofcontents{%
    \chapter*{\contentsname}
        \@mkboth{\MakeUppercase\contentsname}{\MakeUppercase\contentsname}
    \vspace{.7cm}
    {\hfill \bfseries Page\par}     
    \@starttoc{toc}%
    }

\newcommand*\l@chapter[2]{%
  \ifnum \c@tocdepth >\m@ne
    \addpenalty{-\@highpenalty}%
    \vskip 1.0em \@plus\p@
    \setlength\@tempdima{1.5em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      \leavevmode \bfseries
      \hskip -\leftskip
      #1\nobreak\hfil \nobreak #2 \par
      \penalty\@highpenalty
    \endgroup
  \fi}
\def\@dottedtocline#1#2#3#4#5{%
	\ifnum #1>\c@tocdepth \else
	\vskip \z@ \@plus.2\p@
	{\leftskip #2\relax \rightskip \@tocrmarg \parfillskip -\rightskip
		\parindent #2\relax\@afterindenttrue
		\interlinepenalty\@M
		\leavevmode
		\@tempdima #3\relax
		\advance\leftskip \@tempdima \null\nobreak\hskip -\leftskip
		{#4}\nobreak\dotdot\nobreak
		{\normalfont \normalcolor #5}%
		\par}%
	\vspace{-15\p@}
	\fi}
	
\def\@dottedtoclineFT#1#2#3#4#5{%
	\ifnum #1>\c@tocdepth \else
	\vskip \z@ \@plus.2\p@
	{\leftskip #2\relax \rightskip \@tocrmarg \parfillskip -\rightskip
		\parindent #2\relax\@afterindenttrue
		\interlinepenalty\@M
		\leavevmode
		\@tempdima #3\relax
		\advance\leftskip \@tempdima \null\nobreak\hskip -\leftskip
		{#4}\nobreak\dotdot\nobreak
		{\normalfont \normalcolor #5}%
		\par}%
	\vspace{-15\p@}
	\fi
}

\newcommand*\l@section{\@dottedtocline{1}{1.5em}{1.5em}}  
\newcommand*\l@subsection{\@dottedtocline{2}{3em}{2.3em}}
\newcommand*\l@subsubsection{\@dottedtocline{3}{4.5em}{3em}}
\newcommand*\l@paragraph{\@dottedtocline{4}{10em}{5em}}
\newcommand*\l@subparagraph{\@dottedtocline{5}{12em}{6em}}
\newcommand\listoffigures{%
    \chapter*{\listfigurename}%
      \@mkboth{\MakeUppercase\listfigurename}%
              {\MakeUppercase\listfigurename}%
              \vspace{.7cm}
              {\hfill \bfseries Page\par}\vspace{-12\p@}
              \addcontentsline{toc}{part}{\MakeUppercase{\listfigurename}}
    \@starttoc{lof}%
    }
\newcommand*\l@figure{\@dottedtoclineFT{1}{0em}{4.5em}}
\newcommand\listoftables{%
    \chapter*{\listtablename}%
      \@mkboth{%
          \MakeUppercase\listtablename}%
         {\MakeUppercase\listtablename}%
         \vspace{.7cm}
         {\hfill \bfseries Page\par}\vspace{-12\p@}
         \addcontentsline{toc}{part}{\MakeUppercase{\listtablename}}
    \@starttoc{lot}%
    %\clearpage
    %\pagenumbering{arabic}
    }
\newcommand*\l@table{\@dottedtoclineFT{1}{0em}{4.0em}}
\newdimen\bibindent
\setlength\bibindent{1.5em}
\newenvironment{thebibliography}[1]
     {\chapter*{\bibname}%
      \@mkboth{\MakeUppercase\bibname}{\MakeUppercase\bibname}%
      \list{\@biblabel{\@arabic\c@enumiv}}%
           {\settowidth\labelwidth{\@biblabel{#1}}%
            \leftmargin\labelwidth
            \advance\leftmargin\labelsep
            \@openbib@code
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \sloppy
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\def\@noitemerr
       {\@latex@warning{Empty `thebibliography' environment}}%
      \endlist}
\newcommand\newblock{\hskip .11em\@plus.33em\@minus.07em}
\let\@openbib@code\@empty
\newenvironment{theindex}
               {\if@twocolumn
                  \@restonecolfalse
                \else
                  \@restonecoltrue
                \fi
                \twocolumn[\@makeschapterhead{\indexname}]%
                \@mkboth{\MakeUppercase\indexname}%
                        {\MakeUppercase\indexname}%
                \thispagestyle{plain}\parindent\z@
                \parskip\z@ \@plus .3\p@\relax
                \columnseprule \z@
                \columnsep 35\p@
                \let\item\@idxitem}
               {\if@restonecol\onecolumn\else\clearpage\fi}
\newcommand\@idxitem{\par\hangindent 40\p@}
\newcommand\subitem{\@idxitem \hspace*{20\p@}}
\newcommand\subsubitem{\@idxitem \hspace*{30\p@}}
\newcommand\indexspace{\par \vskip 10\p@ \@plus5\p@ \@minus3\p@\relax}
\renewcommand\footnoterule{%
  \kern-3\p@
  \hrule\@width.4\columnwidth
  \kern2.6\p@}
\@addtoreset{footnote}{chapter}
\newcommand\@makefntext[1]{%
    \parindent 1em%
    \noindent
    \hb@xt@1.8em{\hss\@makefnmark}#1}
\newcommand\contentsname{CONTENTS}
\newcommand\listfigurename{LIST OF FIGURES}
\newcommand\listtablename{LIST OF TABLES}
\newcommand\bibname{REFERENCES}
\newcommand\indexname{Index}
\newcommand\figurename{Figure}
\newcommand\tablename{Table}
\newcommand\partname{Part}
\newcommand\chaptername{CHAPTER}
\newcommand\appendixname{APPENDICES}
\newcommand\listofsymbolsname{LIST OF SYMBOLS}
\newcommand\listofabbreviationsname{LIST OF ABBREVIATIONS}
\newcommand\abstractname{Abstract}
\def\today{\ifcase\month\or
  January\or February\or March\or April\or May\or June\or
  July\or August\or September\or October\or November\or December\fi
  \space\number\day, \number\year}
\setlength\columnsep{10\p@}
\setlength\columnseprule{0\p@}
\pagestyle{plain}
\pagenumbering{arabic}
\if@twoside
\else
  \raggedbottom
\fi
\if@twocolumn
  \twocolumn
  \sloppy
  \flushbottom
\else
  \onecolumn
\fi


\def\titleTR#1{\gdef\@titleTR{#1}}
\def\supervisor#1{\gdef\@supervisor{#1}}
\def\cosupervisor#1{\gdef\@cosupervisor{#1}}
\def\comiteememberone#1{\gdef\@comiteememberone{#1}}
\def\comiteemembertwo#1{\gdef\@comiteemembertwo{#1}}
\def\examiningcomiteememberone#1{\gdef\@examiningcomiteememberone{#1}}
\def\examiningcomiteemembertwo#1{\gdef\@examiningcomiteemembertwo{#1}}
\def\examiningcomiteememberthree#1{\gdef\@examiningcomiteememberthree{#1}}
\def\director#1{\gdef\@director{#1}}
\newcommand \Dotdot {\leavevmode \cleaders \hb@xt@ .18em{\hss .\hss }\hfill \kern \z@}
\def \resultform{
	\clearpage
	\chapter*{\center 
		\if@PhD
		Ph.D.
		\else
		M.Sc.
		\fi
	  THESIS EXAMINATION RESULT FORM
  	}
	\addcontentsline{toc}{part}{\if@PhD Ph.D. \else M.Sc. \fi\MakeUppercase{THESIS EXAMINATION RESULT FORM}}
	\vskip0.2in
	\noindent We have read the thesis entitled \textquotedblleft\textbf{\MakeUppercase{\@title}}\textquotedblright\ completed by \textbf{\MakeUppercase{\@author}} under supervision of \textbf{\MakeUppercase{\@supervisor}} 
	\if@withcoadvisor
	and \textbf{\MakeUppercase{\@cosupervisor}} 
	\fi
	and we certify that in our opinion it is fully adequate, in scope and in quality, as a thesis for the degree of 
	\if@PhD
	Doctor of Philosophy.
	\else
	Master of Science.
	\fi
	\if@PhD
			\vspace{0.3cm}
			\vfill
			\if@coadvisorsigned
				\noindent
				\begin{minipage}{.44\textwidth}
					\centering
					\vspace{-2ex}
					\begin{tabular}{c}
						\makebox[\textwidth]{\Dotdot}\\[1ex]
						\@supervisor\\
						\hline\\[-2ex]
						Supervisor\\
						\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
					\end{tabular}
				\end{minipage}
				\hspace{.1\textwidth}
				\begin{minipage}{.44\textwidth}
					\centering
					\vspace{-2ex}
					\begin{tabular}{c}
						\makebox[\textwidth]{\Dotdot}\\[1ex]
						\@cosupervisor\\
						\hline\\[-2ex]
						Co-supervisor\\
						\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
					\end{tabular}
				\end{minipage}
			\else
				\begin{center}
					\makebox[.42\textwidth]{\Dotdot}\\[2ex]
					\begin{tabular}{c}
						\@supervisor\\
						\hline\\[-2ex]
						Supervisor\\
						\null\hfill\rule{.38\textwidth}{0pt}\hfill\null
					\end{tabular}
				\end{center}
			\fi
				\vspace{\signpagevspacewithcoadviser}
				\vfill
				\noindent
				\begin{minipage}{.44\textwidth}
					\centering
					\vspace{-2ex}
					\begin{tabular}{c}
						\makebox[\textwidth]{\Dotdot}\\[1ex]
						\@comiteememberone\\
						\hline\\[-2ex]
						Thesis Committee Member\\
						\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
					\end{tabular}
				\end{minipage}
				\hspace{.1\textwidth}
				\begin{minipage}{.44\textwidth}
					\centering
					\vspace{-2ex}
					\begin{tabular}{c}
						\makebox[\textwidth]{\Dotdot}\\[1ex]
						\@comiteemembertwo\\
						\hline\\[-2ex]
						Thesis Committee Member\\
						\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
					\end{tabular}
				\end{minipage}
			\vspace{\signpagevspacewithcoadviser}
			\vfill
			\noindent
			\begin{minipage}{.44\textwidth}
				\centering
				\vspace{-2ex}
				\begin{tabular}{c}
					\makebox[\textwidth]{\Dotdot}\\[1ex]
					\@examiningcomiteememberone\\
					\hline\\[-2ex]
					Examining Committee Member\\
					\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
				\end{tabular}
			\end{minipage}
			\hspace{.1\textwidth}
			\begin{minipage}{.44\textwidth}
				\centering
				\vspace{-2ex}
				\begin{tabular}{c}
					\makebox[\textwidth]{\Dotdot}\\[1ex]
					\@examiningcomiteemembertwo\\
					\hline\\[-2ex]
					Examining Committee Member\\
					\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
				\end{tabular}
			\end{minipage}
		\if@coadvisorsigned
			\vspace{\signpagevspacewithcoadviser}
			\vfill
			\noindent
			\begin{center}
				\begin{minipage}{.44\textwidth}
					\centering
					\vspace{-2ex}
					\begin{tabular}{c}
						\makebox[\textwidth]{\Dotdot}\\[1ex]
						\@examiningcomiteememberthree\\
						\hline\\[-2ex]
						Examining Committee Member\\
						\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
					\end{tabular}
				\end{minipage}
			\end{center}
		\fi
		
			\vfill
			\begin{center}
				\begin{tabular}{c}
					\null \\
					\hline\\[-3ex]
					\mbox{\@director} \\[-1ex]
					Director \\[-1ex]
					Graduate School of Natural and Applied Sciences\\
				\end{tabular}
			\end{center}
	\else % MSc
			\vspace{0.3cm}
			\vfill
			\if@coadvisorsigned
				\noindent
				\begin{minipage}{.44\textwidth}
					\centering
					\vspace{-2ex}
					\begin{tabular}{c}
						\makebox[\textwidth]{\Dotdot}\\[1ex]
						\@supervisor\\
						\hline\\[-2ex]
						Supervisor\\
						\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
					\end{tabular}
				\end{minipage}
				\hspace{.1\textwidth}
				\begin{minipage}{.44\textwidth}
					\centering
					\vspace{-2ex}
					\begin{tabular}{c}
						\makebox[\textwidth]{\Dotdot}\\[1ex]
						\@cosupervisor\\
						\hline\\[-2ex]
						Co-supervisor\\
						\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
					\end{tabular}
				\end{minipage}
			\else
				\begin{center}
					\vspace{-2ex}
					\begin{tabular}{c}
						\makebox[.42\textwidth]{\Dotdot}\\[2ex]
						\@supervisor\\
						\hline\\[-2ex]
						Yönetici\\
						\null\hfill\rule{.38\textwidth}{0pt}\hfill\null
					\end{tabular}
				\end{center}
			\fi
				\vspace{\signpagevspacewithcoadviser}
				\vfill
				\noindent
				\begin{minipage}{.44\textwidth}
					\centering
					\vspace{-2ex}
					\begin{tabular}{c}
						\makebox[\textwidth]{\Dotdot}\\[1ex]
						\@comiteememberone\\
						\hline\\[-2ex]
						Jury Member\\
						\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
					\end{tabular}
				\end{minipage}
				\hspace{.1\textwidth}
				\begin{minipage}{.44\textwidth}
					\centering
					\vspace{-2ex}
					\begin{tabular}{c}
						\makebox[\textwidth]{\Dotdot}\\[1ex]
						\@comiteemembertwo\\
						\hline\\[-2ex]
						Jury Member\\
						\null\hfill\rule{.86\textwidth}{0pt}\hfill\null
					\end{tabular}
				\end{minipage}
			\vspace{\signpagevspacewithcoadviser}
			\vfill
			\noindent		
			\vfill
			\begin{center}
				\begin{tabular}{c}
					\null \\
					\hline\\[-3ex]
					\mbox{\@director} \\[-1ex]
					Director \\[-1ex]
					Graduate School of Natural and Applied Sciences\\
				\end{tabular}
			\end{center}
	\fi
	
}
\newenvironment{acknowledgment}
  {	
  	\clearpage
  	\chapter*{\center ACKNOWLEDGEMENTS}
  	\addcontentsline{toc}{part}{\MakeUppercase{ACKNOWLEDGEMENTS}}
  	\vskip0.05in
  }
  {
  	\begin{flushright}
  		\@author
  	\end{flushright}
  }
\renewenvironment{abstract}
{	
	\clearpage
	\chapter*{\center \@title \vskip0.05in ABSTRACT}
	\addcontentsline{toc}{part}{\MakeUppercase{ABSTRACT}}	
	\vskip0.2in
}
{

}
\newenvironment{abstractTR}
{	
	\clearpage
	\chapter*{\center \@titleTR \vskip0.05in ÖZ}
	\addcontentsline{toc}{part}{\MakeUppercase{ÖZ}}	
	\vskip0.2in
}
{
	
}

\def\startappendix{
	\clearpage
	\addcontentsline{toc}{chapter}{\protect\MakeUppercase{\appendixname}\dotdot\nobreak}	
	\appendix
	\chapter*{\appendixname}
	\setcounter{chapter}{0}%
	\setcounter{section}{0}%
	\setcounter{figure}{0}%
	\setcounter{table}{0}%
	\setcounter{equation}{0}
	\renewcommand{\theequation}{A.\arabic{equation}}
	\renewcommand{\thechapter}{A.\arabic{section}}
	\renewcommand{\thesection}{A.\arabic{section}}
	\renewcommand{\theAsection}{Appendix \arabic{section}}
	\renewcommand{\thetable}{Table~A.\@arabic\c@table}
	\renewcommand{\thefigure}{Figure~A.\@arabic\c@figure}
}

\def\startlistofabbreviations{
	\clearpage
	\addcontentsline{toc}{part}{\protect\MakeUppercase{\listofabbreviationsname}\dotdot\nobreak}	
	\chapter*{\listofabbreviationsname}
}

\def\startlistofsymbols{
	\clearpage
	\addcontentsline{toc}{part}{\protect\MakeUppercase{\listofsymbolsname}\dotdot\nobreak}	
	\chapter*{\listofsymbolsname}
}

\def \addappendixpart#1{
	\addtocounter{section}{1}
	\addcontentsline{toc}{section}{\thesection :\ #1\protect\leaders\hbox{$\m@th
				\mkern\@dotsep mu\hbox{.}\mkern\@dotsep
				mu$}\hfil}
	\section*{\thesection\protect \; #1}
}

\def\startbibliography{
		\clearpage
		\addcontentsline{toc}{chapter}{\protect\MakeUppercase{\bibname}\dotdot\nobreak}
		\addtocontents{toc}{\vspace{-1.0em}}
		\bibliographystyle{fbe}
	}

\tolerance=1
\emergencystretch=\maxdimen
\hyphenpenalty=10000
\hbadness=10000


\usepackage[comma,authoryear]{natbib}
\usepackage{caption}
\captionsetup[table]{singlelinecheck=false,justification=justified,font={footnotesize,onehalfspacing},labelsep=space}
\captionsetup[figure]{singlelinecheck=true,font={footnotesize,onehalfspacing},labelsep=space}
%\captionsetup[table]{singlelinecheck=false,justification=justified,font=footnotesize,labelsep=space}
%\captionsetup[figure]{singlelinecheck=true,font=footnotesize,labelsep=space}

\endinput